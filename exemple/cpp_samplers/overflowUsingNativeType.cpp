/// Copyright (c) 2016, Jean-Baptiste Laurent
/// All rights reserved.
///
/// Redistribution and use in source and binary forms, with or without
/// modification, are permitted provided that the following conditions are met:
///
/// 1. Redistributions of source code must retain the above copyright notice, this
///    list of conditions and the following disclaimer.
/// 2. Redistributions in binary form must reproduce the above copyright notice,
///    this list of conditions and the following disclaimer in the documentation
///    and/or other materials provided with the distribution.
///
/// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
/// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
/// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
/// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
/// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
/// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
/// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
/// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
/// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
/// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
///
/// The views and conclusions contained in the software and documentation are those
/// of the authors and should not be interpreted as representing official policies,
/// either expressed or implied, of the Barghest Project.
///
/// @brief A simple example of how to use nativeTypeArithmetic in C++.
///

#include <iostream>    // std::cout
#include "NativeType.hh"

/*!
** Use the SF (Sign Flag) in order to tell if the result
** is positive or negative.
*/
void        isNeg(nativeType::t_eflags const &f)
{
  if (f.s_eflags.SF == 1)
    std::cout << "The result is negative" << std::endl;
  else
    std::cout << "The result is positive" << std::endl;
}

/*!
** Perform an addition and check for an overflow if any.
*/
template <typename T>
void            signAddOverflow(nativeType::NativeType<T> a,
                                nativeType::NativeType<T> b)
{
  nativeType::NativeType<T>    result;

  result = a + b;
  if (result.getOverflow()) // No need to check CF/OF flags by hand
    std::cout << "Error: Sign Overflow detected ("
              << +a << " + " << +b << " = " << +result << ") ==> ";
  else
    std::cout << "Success: " << +a << " + " << +b << " = " << +result << " ==> ";
  isNeg(result.getEflagsStruct());
}

/*!
** Perform a multiplication and check for an overflow if any.
*/
template <typename T>
void            unsignMultOverflow(nativeType::NativeType<T> a,
                                   nativeType::NativeType<T> b)
{
  nativeType::NativeType<T>        result;

  result = a * b;
  if (result.getOverflow()) // No need to check CF/OF flags by hand
    std::cout << "Error: Unsigned Overflow detected ("
              << +a << " + " << +b << " = " << +result << ")";
  else
    std::cout << "Success: " << +a << " * " << +b << " = " << +result << std::endl;
}

int    main()
{
  std::cout << "==== Using signAddOverflow ======" << std::endl;
  signAddOverflow<int8_t>(5, 6);
  signAddOverflow<int16_t>(-1552, 6);
  signAddOverflow<int32_t>(-2147483648, -1);
  signAddOverflow<int32_t>(2147483647, 1);
  /**/
  std::cout << std::endl << "==== Using unsignMultOverflow ======" << std::endl;
  unsignMultOverflow<int64_t>(15, 3);
  unsignMultOverflow<uint32_t>(50000000, 4294967);
  std::cout << std::endl << "==== End ======" << std::endl;
  return (0);
}
