/// Copyright (c) 2016, Jean-Baptiste Laurent
/// All rights reserved.
///
/// Redistribution and use in source and binary forms, with or without
/// modification, are permitted provided that the following conditions are met:
///
/// 1. Redistributions of source code must retain the above copyright notice, this
///    list of conditions and the following disclaimer.
/// 2. Redistributions in binary form must reproduce the above copyright notice,
///    this list of conditions and the following disclaimer in the documentation
///    and/or other materials provided with the distribution.
///
/// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
/// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
/// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
/// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
/// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
/// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
/// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
/// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
/// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
/// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
///
/// The views and conclusions contained in the software and documentation are those
/// of the authors and should not be interpreted as representing official policies,
/// either expressed or implied, of the Barghest Project.
///
/// @brief An example of a real world application of the NativeType class.
/// boost::interval<> is a class that allows to handle mathematical interval.
/// These interval [min, max] are design for a mathematical use, and not an
/// informatic one that include, for example, type limitation. boost::interval<>
/// does not handle overflow during the computation of min/max.
/// The code below want to check the range of an integer variable over
/// some arithmetics and ensure there is no overflow. The context behind
/// concern static code and variable analysis.
///
/// Compile with: g++ -std=c++11 boostIntervalOverflow.cpp -I ../../include/ -L ../../build/ -lnativeTypeArithmetic

#include <iostream>        // std::cout
#include <cstdlib>        // std::rand()
#include <boost/numeric/interval.hpp>    // boost::numeric::interval_lib::interval
#include "NativeType.hh"    // NativeType

using namespace boost::numeric;
using namespace interval_lib;

/***************************/

#define TYPE_T        int8_t
// #define TYPE_T        int16_t
// #define TYPE_T        uint32_t

static const TYPE_T    varA_start = 42; // Min value of A
static const TYPE_T    varA_rand = 16;    // Variation of A
static const TYPE_T    varB_start = 1;    // Min value of B
static const TYPE_T    varB_rand = 4;    // Variation of B

/*!
** Is there an oveflow on this code ?
** When is there a risk ? Which loop iteration ?
*/
void    analysedCode()
{
  char    a;
  char    b;

  a = varA_start + (std::rand() % varA_rand + 1);
  b = varB_start + (std::rand() % varB_rand + 1);
  while (1)
    a += b;
}

/***************************/

template <typename T>
void    printInterval(std::ostream &os, interval<T> const &inter)
{
  os << "[" << +(TYPE_T)inter.lower() << ", "<< +(TYPE_T)inter.upper() << "]";
}

/*!
** Instrument analyseCode and check if there is a possible overflow.
** We use NativeType because boost does not keep track of overflow during
** the computation of the lower and higher bound.
*/
void    checkingAnalysedCode()
{
  unsigned int    loopIt;
  interval<nativeType::NativeType<TYPE_T> >    interA; /* int8_t = [-128, +127] */
  interval<nativeType::NativeType<TYPE_T> >    interB;

  loopIt = 0;
  interA.assign(varA_start, varA_start + varA_rand);    /* [42, 58] */
  interB.assign(varB_start, varB_start + varB_rand);        /* [  1, 5] */
  while (interA.lower().getOverflow() == false
         && interA.upper().getOverflow() == false)
  {
    loopIt++;
    std::cout << "Iteration " << loopIt << ": Perform";
    printInterval(std::cout, interA);
    std::cout << " + ";
    printInterval(std::cout, interB);
    std::cout << std::endl;
    interA += interB;    /* [minA+minB, maxA+maxB] */
  }
  std::cout << "Warning: Overflow detected at loop iteration " << loopIt << std::endl;
  std::cout << "Resulting interval:";
  printInterval(std::cout, interA);
  std::cout << std::endl;
}

int    main()
{
  checkingAnalysedCode();
  return (0);
}
