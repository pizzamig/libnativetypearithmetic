// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
///
/// @brief The definition of the eflags structure.

#ifndef NATIVE_ARITH_EFLAGS_HH_
# define NATIVE_ARITH_EFLAGS_HH_

# include <limits>    // std::numeric_limits<T>::is_signed

# ifdef __cplusplus
namespace nativeType
{
  extern "C"
  {
# endif /* !__cplusplus */

/*!
** A POD structure corresponding to the 16 lower bits of the eflags register
** returned by the following function.
*/
    typedef union u_eflags
    {
      struct
      {
        uint16_t    CF:1; /* CF (bit 0) Carry flag */
        uint16_t    B1:1; /* Reserved bit positions */
        uint16_t    PF:1; /* PF (bit 2) Parity flag */
        uint16_t    B3:1; /* Reserved bit positions */
        uint16_t    AF:1; /* AF (bit 4) Adjust flag */
        uint16_t    B5:1; /* Reserved bit positions */
        uint16_t    ZF:1; /* ZF (bit 6) Zero flag */
        uint16_t    SF:1; /* SF (bit 7) Sign flag */
        uint16_t    TF:1; /* TF (bit 8) Trap Flag */
        uint16_t    IF:1; /* IF (bit 9) Interrupt Enable Flag */
        uint16_t    DF:1; /* DF (bit 10) Direction Flag  */
        uint16_t    OF:1; /* OF (bit 11) Overflow flag */
        uint16_t    IOPL:2; /* IOPL (bit 12/13) I/O Privilege Level */
        uint16_t    NT:1; /* NT (bit 14) Nested Task */
        uint16_t    B15:1; /* Reserved bit positions */
      } s_eflags;    /* Note: ISO C++ prohibits anonymous structs */
      uint16_t      word;
# ifdef __cplusplus

      /*!
      ** Reset the state of all the flag.
      */
      void    clear(void) {this->word = 0;}
# endif /* !__cplusplus */
    } t_eflags;

// # ifdef __cplusplus
// static_assert(sizeof(t_eflags) == 2 && std::is_pod<t_eflags>::value, "Must be a 2bytes POD");
// # endif /* !__cplusplus */

# ifdef __cplusplus
  } // !extern "C"

  /*!
  ** @return true if there is an overflow/underflow, false otherwise.
  ** It automaticaly choose between OF and CF depending on the sign.
  */
  template <typename T>
  bool    getOverflow(t_eflags const &f)
  {
    return (std::numeric_limits<T>::is_signed ? f.s_eflags.OF : f.s_eflags.CF);
  }

} // !namespace nativeType
# endif /* !__cplusplus */

#endif /* !NATIVE_ARITH_EFLAGS_HH_ */
