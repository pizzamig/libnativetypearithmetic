// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//
/// @brief Definition of the class NativeTypeCommon, parent class from NativeType
///        and containing all method not depending from the templated type <T>.
///

#ifndef NATIVE_TYPE_COMMON_HH_
# define NATIVE_TYPE_COMMON_HH_

# include "cCppNativeArithmeticWithEflags.hh"    // typedef union u_eflags t_eflags;

namespace nativeType
{

  /*!
  ** Higher level class for NativeType. It contains all the method not depending
  ** of the template type <T>. This class also provides two things:
  ** ** * -> A micro management of the "overflow" flags spread and kept all along
  **         the operation performed.
  **         The "overflow" word is taken as a general term which here
  **         includes the overflow and the underflow for both signed and unsigned
  **         variable.
  ** ** * -> A direct access to the eflags of the operation. In contrari to the
  **         above, the eflags are reset (set all flag to false) before filling
  **         them with the result given by the assembly instruction.
  **
  ** This class provides getter/setter for the overflow boolean information and
  ** allows the NativeType class to contains only method using the template type T.
  ** (and therefor speedup the compilation process).
  ** Note: This class is not intended to be used directly.
  */
  class NativeTypeCommon
  {
  public:
    //! @return true if an "overflow" occured, false otherwise
    bool    getOverflow(void) const;
    //! Set the overflow status
    void    setOverflow(bool);
    //! Reset the overflow status to false.
    void    clrOverflow(void);
    //! @brief Return the overflow status and clear the flag.
    //! @return The overflow status.
    bool    getAndClrOverflow(void);

    //! Set the eflags stored
    void    setEflags(uint16_t f);
    void    setEflags(t_eflags const &f);
    //! @return the eflags of the last operation.
    uint16_t        getEflags(void) const;
    t_eflags const    &getEflagsStruct(void) const;
    //! @brief Reset the content of the eflags. (all to false)
    void    clrEflags(void);

    // NativeTypeCommon    &operator=(NativeTypeCommon const &other) = default;
  protected:
    //! Default constructor
    NativeTypeCommon(void);
    // NativeTypeCommon(NativeTypeCommon const&) = default;
    // NativeTypeCommon(NativeTypeCommon &&) = default;
  private:
    //! A boolean used to keep track of overflow during interval arithmetic
    bool    _overflow;
    //! The eflags returned by the last operation performed.
    t_eflags    _eflags;
  }; // !class NativeTypeCommon

} // !namespace nativeType

#endif /* ! NATIVE_TYPE_COMMON_HH_ */
