// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
//
// @brief C/C++ arithmetics wrapper to check eflags
// This file contain prototype on function allowing a programmer to perform
/// intel assembly arithmetic and have direct access of the 16 lower bits of
/// the eflags register (for example, to check the Overflow or the Parity).
///
/// All those functions follow a simple prototype:
/// @param dst The first operand of the operation 'op'
/// @param src The second operand of the operation 'op'
/// @param *result A pointer pointing to the result of the operation between dst and src
/// @param *quotient In case the function is a division, this parameter is a pointer
///                 where the quotient is stored. If NULL the quotient is ignored.
/// @param *remainder In case the function is a division, this parameter is a pointer
///                 where the remainder is stored. If NULL the remainder is ignored.
/// @param high Multiplication: this parameter is a pointer where the Intel assembly
///             high part of the result is stored. (cf mul/imul Intel manual)
/// @param high Division: this parameter is the high part of the division.
///             (cf mul/imul Intel manual). If not specified it is sign extended,
///             (if <T> is signed) or reset to zero for unsigned.
/// @return The 16 lower bits of the intel assembly eflags register. Per default
///         all flags are set to '0' before the arithmetic instruction.
/// Exemple: * uint16_t    native[op][(Sign/Unsigned)](dst, src, *result);
///
/// To simplify the usage of the return value, the user can cast the uint16_t
/// returned into the "t_eflags" or use the union .word attribute.
/// Internal note: Those functions do not return directly a t_eflags because the
/// resulting assembly code (done with a lot of love) will be too complex, decreasing
/// maintainability and portability (as OS and Compiler have different way to return a
/// struct type, even if this type fit into the ax/eax/rax register).
///
/// /!\ Warning /!\ For the division there are 2 cases where floating point
/// exceptions are raised: A division by zero, or an overflow (for signed div)
/// which occured if the minimum is divided by -1. (Ex: (int8_t)-128/-1). Its
/// up to the user to check the division parameters before.
///

#ifndef C_CPP_WRAPPER_WITH_EFLAGS_HH_
# define C_CPP_WRAPPER_WITH_EFLAGS_HH_

//! Inclusion of type with a precise size
# include <stdint.h>        // ([u]int*_t)
# ifdef __cplusplus
// #  include <type_traits>    // static_assert<>()
#  include <iosfwd>        // std::ostream
# endif /* !__cplusplus */

# include "eflags.hh"        // t_eflags

# ifdef __cplusplus
namespace nativeType
{
# endif /* !__cplusplus */

//! Signed/Unsigned on integer doc (Intel man p83)
//! Notes: In contrarily to "imul" and "mul" instrunction,
//! "add" and "sub" are both signed and unsigned instruction.
//! OF and CF flag can be used for signed and unsigned arithmetic
//! respectively.
# ifdef __cplusplus
  extern "C"
  {
# else /* C */
# endif /* !__cplusplus */
/* Signed add/sub */
    uint16_t nativeAddInt8(int8_t, int8_t, int8_t *result);
    uint16_t nativeAddInt16(int16_t, int16_t, int16_t *result);
    uint16_t nativeAddInt32(int32_t, int32_t, int32_t *result);

    uint16_t nativeSubInt8(int8_t, int8_t, int8_t *result);
    uint16_t nativeSubInt16(int16_t, int16_t, int16_t *result);
    uint16_t nativeSubInt32(int32_t, int32_t, int32_t *result);
/* Unsigned add/sub */
    uint16_t nativeAddUInt8(uint8_t, uint8_t, uint8_t *result);
    uint16_t nativeAddUInt16(uint16_t, uint16_t, uint16_t *result);
    uint16_t nativeAddUInt32(uint32_t, uint32_t, uint32_t *result);

    uint16_t nativeSubUInt8(uint8_t, uint8_t, uint8_t *result);
    uint16_t nativeSubUInt16(uint16_t, uint16_t, uint16_t *result);
    uint16_t nativeSubUInt32(uint32_t, uint32_t, uint32_t *result);

/* Signed multiplication */
    uint16_t nativeMultInt8(int8_t, int8_t, int8_t *result);
    uint16_t nativeMultInt16(int16_t, int16_t, int16_t *result);
    uint16_t nativeMultInt32(int32_t, int32_t, int32_t *result);
    uint16_t nativeMultHighInt8(int8_t, int8_t, int8_t *result, int8_t *high);
    uint16_t nativeMultHighInt16(int16_t, int16_t, int16_t *result, int16_t *high);
    uint16_t nativeMultHighInt32(int32_t, int32_t, int32_t *result, int32_t *high);
/* Unsigned multiplication */
    uint16_t nativeMultUInt8(uint8_t, uint8_t, uint8_t *result);
    uint16_t nativeMultUInt16(uint16_t, uint16_t, uint16_t *result);
    uint16_t nativeMultUInt32(uint32_t, uint32_t, uint32_t *result);
    uint16_t nativeMultHighUInt8(uint8_t, uint8_t, uint8_t *result, uint8_t *high);
    uint16_t nativeMultHighUInt16(uint16_t, uint16_t, uint16_t *result, uint16_t *high);
    uint16_t nativeMultHighUInt32(uint32_t, uint32_t, uint32_t *result, uint32_t *high);

/* Signed multiplication */
    uint16_t nativeDivInt8(int8_t, int8_t, int8_t *quotient, int8_t *remainder);
    uint16_t nativeDivInt16(int16_t, int16_t, int16_t *quotient, int16_t *remainder);
    uint16_t nativeDivInt32(int32_t, int32_t, int32_t *quotient, int32_t *remainder);
    uint16_t nativeDivHighInt8(int8_t, int8_t, int8_t *quotient, int8_t *remainder, int8_t high);
    uint16_t nativeDivHighInt16(int16_t, int16_t, int16_t *quotient, int16_t *remainder, int16_t high);
    uint16_t nativeDivHighInt32(int32_t, int32_t, int32_t *quotient, int32_t *remainder, int32_t high);
/* Unsigned multiplication */
    uint16_t nativeDivUInt8(uint8_t, uint8_t, uint8_t *quotient, uint8_t *remainder);
    uint16_t nativeDivUInt16(uint16_t, uint16_t, uint16_t *quotient, uint16_t *remainder);
    uint16_t nativeDivUInt32(uint32_t, uint32_t, uint32_t *quotient, uint32_t *remainder);
    uint16_t nativeDivHighUInt8(uint8_t, uint8_t, uint8_t *quotient, uint8_t *remainder, uint8_t high);
    uint16_t nativeDivHighUInt16(uint16_t, uint16_t, uint16_t *quotient, uint16_t *remainder, uint16_t high);
    uint16_t nativeDivHighUInt32(uint32_t, uint32_t, uint32_t *quotient, uint32_t *remainder, uint32_t high);

/* Left shift (signed and unsigned) */
    uint16_t nativeLShiftInt8(int8_t, int8_t, int8_t *result);
    uint16_t nativeLShiftInt16(int16_t, int16_t, int16_t *result);
    uint16_t nativeLShiftInt32(int32_t, int32_t, int32_t *result);
    uint16_t nativeLShiftUInt8(uint8_t, uint8_t, uint8_t *result);
    uint16_t nativeLShiftUInt16(uint16_t, uint16_t, uint16_t *result);
    uint16_t nativeLShiftUInt32(uint32_t, uint32_t, uint32_t *result);

/* Right shift (signed) */
    uint16_t nativeRShiftInt8(int8_t, int8_t, int8_t *result);
    uint16_t nativeRShiftInt16(int16_t, int16_t, int16_t *result);
    uint16_t nativeRShiftInt32(int32_t, int32_t, int32_t *result);
/* Right shift (unsigned) */
    uint16_t nativeRShiftUInt8(uint8_t, uint8_t, uint8_t *result);
    uint16_t nativeRShiftUInt16(uint16_t, uint16_t, uint16_t *result);
    uint16_t nativeRShiftUInt32(uint32_t, uint32_t, uint32_t *result);

/* Binary operator AND '&' (signed and unsigned) */
    uint16_t nativeBitAndInt8(int8_t, int8_t, int8_t *result);
    uint16_t nativeBitAndInt16(int16_t, int16_t, int16_t *result);
    uint16_t nativeBitAndInt32(int32_t, int32_t, int32_t *result);
    uint16_t nativeBitAndUInt8(uint8_t, uint8_t, uint8_t *result);
    uint16_t nativeBitAndUInt16(uint16_t, uint16_t, uint16_t *result);
    uint16_t nativeBitAndUInt32(uint32_t, uint32_t, uint32_t *result);
/* Binary operator OR '|' (signed and unsigned) */
    uint16_t nativeBitOrInt8(int8_t, int8_t, int8_t *result);
    uint16_t nativeBitOrInt16(int16_t, int16_t, int16_t *result);
    uint16_t nativeBitOrInt32(int32_t, int32_t, int32_t *result);
    uint16_t nativeBitOrUInt8(uint8_t, uint8_t, uint8_t *result);
    uint16_t nativeBitOrUInt16(uint16_t, uint16_t, uint16_t *result);
    uint16_t nativeBitOrUInt32(uint32_t, uint32_t, uint32_t *result);
/* Binary operator XOR '^' (signed and unsigned) */
    uint16_t nativeBitXorInt8(int8_t, int8_t, int8_t *result);
    uint16_t nativeBitXorInt16(int16_t, int16_t, int16_t *result);
    uint16_t nativeBitXorInt32(int32_t, int32_t, int32_t *result);
    uint16_t nativeBitXorUInt8(uint8_t, uint8_t, uint8_t *result);
    uint16_t nativeBitXorUInt16(uint16_t, uint16_t, uint16_t *result);
    uint16_t nativeBitXorUInt32(uint32_t, uint32_t, uint32_t *result);

/* 32b/64b compatibility */
# if defined(__x86_64__) || defined(__ia64__) || defined(_M_X64)
    uint16_t nativeAddInt64(int64_t, int64_t, int64_t *result);
    uint16_t nativeSubInt64(int64_t, int64_t, int64_t *result);
    uint16_t nativeAddUInt64(uint64_t, uint64_t, uint64_t *result);
    uint16_t nativeSubUInt64(uint64_t, uint64_t, uint64_t *result);
    uint16_t nativeMultInt64(int64_t, int64_t, int64_t *result);
    uint16_t nativeMultUInt64(uint64_t, uint64_t, uint64_t *result);
    uint16_t nativeMultHighInt64(int64_t, int64_t, int64_t *result, int64_t *high);
    uint16_t nativeMultHighUInt64(uint64_t, uint64_t, uint64_t *result, uint64_t *high);

    uint16_t nativeLShiftInt64(int64_t, int64_t, int64_t *result);
    uint16_t nativeLShiftUInt64(uint64_t, uint64_t, uint64_t *result);
    uint16_t nativeRShiftInt64(int64_t, int64_t, int64_t *result);
    uint16_t nativeRShiftUInt64(uint64_t, uint64_t, uint64_t *result);

    uint16_t nativeBitAndInt64(int64_t, int64_t, int64_t *result);
    uint16_t nativeBitAndUInt64(uint64_t, uint64_t, uint64_t *result);
    uint16_t nativeBitOrInt64(int64_t, int64_t, int64_t *result);
    uint16_t nativeBitOrUInt64(uint64_t, uint64_t, uint64_t *result);
    uint16_t nativeBitXorInt64(int64_t, int64_t, int64_t *result);
    uint16_t nativeBitXorUInt64(uint64_t, uint64_t, uint64_t *result);

    uint16_t nativeDivInt64(int64_t, int64_t, int64_t *quotient, int64_t *remainder);
    uint16_t nativeDivUInt64(uint64_t, uint64_t, uint64_t *quotient, uint64_t *remainder);
    uint16_t nativeDivHighInt64(int64_t, int64_t, int64_t *quotient, int64_t *remainder, int64_t high);
    uint16_t nativeDivHighUInt64(uint64_t, uint64_t, uint64_t *quotient, uint64_t *remainder, uint64_t high);

# endif // !64b
# ifdef __cplusplus
  } // !extern "C"

//////////////////////////////////
// Assembly C++ template function for overflow detection
//! Overload of the add function (add)
  template <typename T> uint16_t add(T a, T b, T *result = NULL);
//! Overload of the sub function (sub)
  template <typename T> uint16_t sub(T a, T b, T *result = NULL);
//! Overload of the mul function (mul/imul)
  template <typename T> uint16_t mul(T a, T b, T *result = NULL);
  template <typename T> uint16_t mul(T a, T b, T *result, T *high);
//! Overload of the lshift function (sal/shl)
  template <typename T> uint16_t lshift(T a, T b, T *result = NULL);
//! Overload of the right function (sar/shr)
  template <typename T> uint16_t rshift(T a, T b, T *result = NULL);

//! Overload of the binary xor (sar/shr)
  template <typename T> uint16_t bitXor(T a, T b, T *result = NULL);
//! Overload of the binary or (sar/shr)
  template <typename T> uint16_t bitOr(T a, T b, T *result = NULL);
//! Overload of the binary and (sar/shr)
  template <typename T> uint16_t bitAnd(T a, T b, T *result = NULL);
//! Overload of the div function (div/idiv)
  template <typename T> uint16_t div(T a, T b, T *quotient = NULL);
  template <typename T> uint16_t div(T a, T b, T *quotient, T *remainder);
  template <typename T> uint16_t div(T a, T b, T *quotient, T *remainder, T high);
//! Overload of the div function (div/idiv) but returing the remainder as third argument
  template <typename T> uint16_t mod(T a, T b, T *remainder = NULL);
} // !namespace nativeType

std::ostream   &operator<<(std::ostream &a, const nativeType::t_eflags &eflags);

# endif /* !__cplusplus */

#endif /* ! C_CPP_WRAPPER_WITH_EFLAGS_HH_ */
