# NativeTypeArithmetic

## What is NativeTypeArithmetic

NativeTypeArithmetic is a library used to access the Intel (x86/x86_64) CPU eflags
after any operation (+, -, *, /, %, <<, >>, &, |). It also allows the user to
handle directly assembly operations via C/C++ (like specifying the high part of a
division).

Practical examples can be found in the example/ directory.

## Context

NativeTypeArithmetic is developed as part of the Barghest project (barghest.org).
