// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
///
/// @brief Implementation of operator working with NativeType

# include "NativeTypeOperator.hh"
# include "cCppNativeArithmeticWithEflags.hh"

/////////////////////////////////////
// Operator overload for arithmetique with the NativeType class

// template <typename T>
// std::ostream    &operator<<(std::ostream &a,
//                 const NativeType<T> &other)
// {
//   a << other.getValue();
//   return (a);
// }

namespace nativeType
{

  //! Perform the operation defined in "arithmetic_fct" on a and b, and return the result.
  //! This function is a merge of the operator+()/operator-()/operator*()/etc. It use a third
  //! parameter in order to call the arithmetic function add/sub and mul respectively.
  //! @param a The first operand of the operation
  //! @param b The second operand of the operation
  //! @param arithmetic_fct The function who perform the operation (using a, b,
  //!                       and a pointer to store the result)
  //! @return A NativeType<T> containing the result of the operation between "a" and "b".
  //!         The overflow is spread if "a" or "b" ensured an overflow which
  //!         was not cleared.
  template <typename T>
  NativeType<T>    PerformOperation(NativeType<T> const &a,
                                    NativeType<T> const &b,
                                    uint16_t (*arithmetic_fct)(T const, T const, T *))
  {
    t_eflags        eflags;
    T               result;
    NativeType<T>   ret;

    eflags.word = arithmetic_fct(a.getValue(), b.getValue(), &result);
    ret.setValue(result);
    ret.setEflags(eflags);
    ret.setOverflow(getOverflow<T>(eflags) || a.getOverflow() || b.getOverflow());
    return (ret);
  }

//! Generate code for the function of native type
# define _GEN_OP_FUNCTION(op, fct)              \
  template <typename T>                         \
  NativeType<T>    op(NativeType<T> const &a,   \
                      NativeType<T> const &b)   \
  {                                             \
    return (PerformOperation(a, b, &fct<T>));   \
  }

  _GEN_OP_FUNCTION(operator+, add)
  _GEN_OP_FUNCTION(operator-, sub)
  _GEN_OP_FUNCTION(operator*, mul)
  _GEN_OP_FUNCTION(operator<<, lshift)
  _GEN_OP_FUNCTION(operator>>, rshift)
  _GEN_OP_FUNCTION(operator^, bitXor)
  _GEN_OP_FUNCTION(operator|, bitOr)
  _GEN_OP_FUNCTION(operator&, bitAnd)
  _GEN_OP_FUNCTION(operator/, div)

  /**********************//**********************/
  /* Forward instantiation */
  /**********************//**********************/

  // Macros below are used to avoid writing the following code by hand for each
  // operator.
  //////////////////////////////////////////////////////////////////////////////////
  //   template NativeType<int8_t> operator-<int8_t>(NativeType<int8_t> const &a,
  //                         NativeType<int8_t> const &b);
  //   template NativeType<int16_t> operator-<int16_t>(NativeType<int16_t> const &a,
  //                           NativeType<int16_t> const &b);
  //   template NativeType<int32_t> operator-<int32_t>(NativeType<int32_t> const &a,
  //                           NativeType<int32_t> const &b);
  // #if defined(__x86_64__) || defined(__ia64__)
  //   template NativeType<int64_t> operator-<int64_t>(NativeType<int64_t> const &a,
  //                           NativeType<int64_t> const &b);
  // #endif /* !64b */

  // /* Unsigned operator-() */

  //   template NativeType<uint8_t> operator-<uint8_t>(NativeType<uint8_t> const &a,
  //                           NativeType<uint8_t> const &b);
  //   template NativeType<uint16_t> operator-<uint16_t>(NativeType<uint16_t> const &a,
  //                             NativeType<uint16_t> const &b);
  //   template NativeType<uint32_t> operator-<uint32_t>(NativeType<uint32_t> const &a,
  //                             NativeType<uint32_t> const &b);
  // #if defined(__x86_64__) || defined(__ia64__)
  //   template NativeType<uint64_t> operator-<uint64_t>(NativeType<uint64_t> const &a,
  //                             NativeType<uint64_t> const &b);
  // #endif /* !64b */
  //////////////////////////////////////////////////////////////////////////////////

  /*!
  ** Generate code for a single type forward instantiation (signed and unsigned)
  */
#define __GEN_SINGLE_FORWARD_INSTANTIATION(op, type, utype)         \
  template NativeType<type> op<type>(NativeType<type> const &a,     \
                                     NativeType<type> const &b);    \
  template NativeType<utype> op<utype>(NativeType<utype> const &a,  \
                                       NativeType<utype> const &b);

  /*!
  ** Generate code for all x86 (32b) forward instantiation.
  */
#define _GEN_FORWARD_INSTANTIATION_X86(op)                  \
  __GEN_SINGLE_FORWARD_INSTANTIATION(op, int8_t, uint8_t)   \
  __GEN_SINGLE_FORWARD_INSTANTIATION(op, int16_t, uint16_t) \
  __GEN_SINGLE_FORWARD_INSTANTIATION(op, int32_t, uint32_t)

#if defined(__x86_64__) || defined(__ia64__) || defined(_M_X64)
  /*!
  ** Generate code for all x86_64 (64b) forward instantiation, if any.
  */
# define _GEN_FORWARD_INSTANTIATION_X86_64(op) _GEN_FORWARD_INSTANTIATION_X86(op) \
  __GEN_SINGLE_FORWARD_INSTANTIATION(op, int64_t, uint64_t)

# define _GEN_FORWARD_INSTANTIATION(op)    _GEN_FORWARD_INSTANTIATION_X86_64(op)
#else /* 32b */
# define _GEN_FORWARD_INSTANTIATION(op)    _GEN_FORWARD_INSTANTIATION_X86(op)
#endif /* ! 64b */

  /* See above for an example of code generated */
  _GEN_FORWARD_INSTANTIATION(operator+)
  _GEN_FORWARD_INSTANTIATION(operator-)
  _GEN_FORWARD_INSTANTIATION(operator*)
  _GEN_FORWARD_INSTANTIATION(operator<<)
  _GEN_FORWARD_INSTANTIATION(operator>>)
  _GEN_FORWARD_INSTANTIATION(operator^)
  _GEN_FORWARD_INSTANTIATION(operator|)
  _GEN_FORWARD_INSTANTIATION(operator&)
  _GEN_FORWARD_INSTANTIATION(operator/)

} // !namespace nativeType
