// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
///
/// @brief C++ template function to wrap C function (32b/64b)
///

#include <iostream>    // std::ostream
#include "cCppNativeArithmeticWithEflags.hh"

static void   *_nullptr; // static => NULL by default

/* Gen code for common function (add/sub/etc) */
# define _GEN_SINGLE_FUNC_3(op_name, type_size, type, fct)  \
  template <> uint16_t op_name(type a, type b,              \
                               type *result){               \
    return (fct##type_size(a, b, result));}
/* Div special case (for the prototype to be the same as the others) */
# define _GEN_SINGLE_FUNC_DIV_3(op_name, type_size, type, fct)  \
  template <> uint16_t op_name(type a, type b,                  \
                               type *result){                   \
    type    *null_ptr = (type *)_nullptr;                       \
    return (fct##type_size(a, b, result, null_ptr));}
/* Modulo special case */
# define _GEN_SINGLE_FUNC_MOD_3(op_name, type_size, type, fct)  \
  template <> uint16_t op_name(type a, type b,                  \
                               type *result){                   \
    type    *null_ptr = (type *)_nullptr;                       \
    return (fct##type_size(a, b, null_ptr, result));}
/* Mul and div gen */
# define _GEN_SINGLE_FUNC_4(op_name, type_size, type, fct)      \
  template <> uint16_t op_name(type a, type b,                  \
                               type *result, type *result2){    \
    return (fct##type_size(a, b, result, result2));}
/* Div last case gen */
# define _GEN_SINGLE_FUNC_DIV_5(op_name, type_size, type, fct)  \
  template <> uint16_t op_name(type a, type b,                  \
                               type *result, type *result2,     \
                               type high){                      \
    return (fct##type_size(a, b, result, result2, high));}

/*
** Generate code for x86 (32b) functions
*/
/* 3 arguments function gen "fct(T a, T b, T *result)" */
#define _GEN_ARITH_FUNC_X86_3(op, signFct, unsignFct)   \
  _GEN_SINGLE_FUNC_3(op, 8, int8_t, signFct)            \
  _GEN_SINGLE_FUNC_3(op, 16, int16_t, signFct)          \
  _GEN_SINGLE_FUNC_3(op, 32, int32_t, signFct)          \
  _GEN_SINGLE_FUNC_3(op, 8, uint8_t, unsignFct)         \
  _GEN_SINGLE_FUNC_3(op, 16, uint16_t, unsignFct)       \
  _GEN_SINGLE_FUNC_3(op, 32, uint32_t, unsignFct)

/* 3 arguments function gen (used by div only) */
#define _GEN_ARITH_FUNC_X86_DIV_3(op, signFct, unsignFct)   \
  _GEN_SINGLE_FUNC_DIV_3(op, 8, int8_t, signFct)            \
  _GEN_SINGLE_FUNC_DIV_3(op, 16, int16_t, signFct)          \
  _GEN_SINGLE_FUNC_DIV_3(op, 32, int32_t, signFct)          \
  _GEN_SINGLE_FUNC_DIV_3(op, 8, uint8_t, unsignFct)         \
  _GEN_SINGLE_FUNC_DIV_3(op, 16, uint16_t, unsignFct)       \
  _GEN_SINGLE_FUNC_DIV_3(op, 32, uint32_t, unsignFct)
/* 3 arguments function gen (used by mod only) */
#define _GEN_ARITH_FUNC_X86_MOD_3(op, signFct, unsignFct)   \
  _GEN_SINGLE_FUNC_MOD_3(op, 8, int8_t, signFct)            \
  _GEN_SINGLE_FUNC_MOD_3(op, 16, int16_t, signFct)          \
  _GEN_SINGLE_FUNC_MOD_3(op, 32, int32_t, signFct)          \
  _GEN_SINGLE_FUNC_MOD_3(op, 8, uint8_t, unsignFct)         \
  _GEN_SINGLE_FUNC_MOD_3(op, 16, uint16_t, unsignFct)       \
  _GEN_SINGLE_FUNC_MOD_3(op, 32, uint32_t, unsignFct)

/* 4 arguments (used by div and mul) "fct(T a, T b, T *quotient, T *remainder)" */
#define _GEN_ARITH_FUNC_X86_4(op, signFct, unsignFct)   \
  _GEN_SINGLE_FUNC_4(op, 8, int8_t, signFct)            \
  _GEN_SINGLE_FUNC_4(op, 16, int16_t, signFct)          \
  _GEN_SINGLE_FUNC_4(op, 32, int32_t, signFct)          \
  _GEN_SINGLE_FUNC_4(op, 8, uint8_t, unsignFct)         \
    _GEN_SINGLE_FUNC_4(op, 16, uint16_t, unsignFct)     \
  _GEN_SINGLE_FUNC_4(op, 32, uint32_t, unsignFct)
/* 5 arguments (used by div only) "fct(T a, T b, T *quotient, T *remainder, T high)" */
#define _GEN_ARITH_FUNC_X86_DIV_5(op, signFct, unsignFct)   \
  _GEN_SINGLE_FUNC_DIV_5(op, 8, int8_t, signFct)            \
    _GEN_SINGLE_FUNC_DIV_5(op, 16, int16_t, signFct)        \
  _GEN_SINGLE_FUNC_DIV_5(op, 32, int32_t, signFct)          \
    _GEN_SINGLE_FUNC_DIV_5(op, 8, uint8_t, unsignFct)       \
  _GEN_SINGLE_FUNC_DIV_5(op, 16, uint16_t, unsignFct)       \
  _GEN_SINGLE_FUNC_DIV_5(op, 32, uint32_t, unsignFct)

/* Generate code for x86_64 (64b) functions */
#define _GEN_ARITH_FUNC_X86_64_3(op, signFct, unsignFct)    \
  _GEN_SINGLE_FUNC_3(op, 64, int64_t, signFct)              \
  _GEN_SINGLE_FUNC_3(op, 64, uint64_t, unsignFct)
#define _GEN_ARITH_FUNC_X86_64_DIV_3(op, signFct, unsignFct)    \
  _GEN_SINGLE_FUNC_DIV_3(op, 64, int64_t, signFct)              \
  _GEN_SINGLE_FUNC_DIV_3(op, 64, uint64_t, unsignFct)
#define _GEN_ARITH_FUNC_X86_64_MOD_3(op, signFct, unsignFct)    \
  _GEN_SINGLE_FUNC_MOD_3(op, 64, int64_t, signFct)              \
  _GEN_SINGLE_FUNC_MOD_3(op, 64, uint64_t, unsignFct)
#define _GEN_ARITH_FUNC_X86_64_4(op, signFct, unsignFct)    \
  _GEN_SINGLE_FUNC_4(op, 64, int64_t, signFct)              \
    _GEN_SINGLE_FUNC_4(op, 64, uint64_t, unsignFct)
#define _GEN_ARITH_FUNC_X86_64_DIV_5(op, signFct, unsignFct)    \
  _GEN_SINGLE_FUNC_DIV_5(op, 64, int64_t, signFct)              \
  _GEN_SINGLE_FUNC_DIV_5(op, 64, uint64_t, unsignFct)

/*****************************************************************************/

/* 32b/64b compatibility */
#if defined(__x86_64__) || defined(__ia64__) || defined(_M_X64)
# define _GEN_ARITH_FUNC_3(op, signFct, unsignFct) _GEN_ARITH_FUNC_X86_3(op, signFct, unsignFct) \
  _GEN_ARITH_FUNC_X86_64_3(op, signFct, unsignFct)
# define _GEN_ARITH_FUNC_DIV_3(op, signFct, unsignFct) _GEN_ARITH_FUNC_X86_DIV_3(op, signFct, unsignFct) \
  _GEN_ARITH_FUNC_X86_64_DIV_3(op, signFct, unsignFct)
# define _GEN_ARITH_FUNC_MOD_3(op, signFct, unsignFct) _GEN_ARITH_FUNC_X86_MOD_3(op, signFct, unsignFct) \
  _GEN_ARITH_FUNC_X86_64_MOD_3(op, signFct, unsignFct)
# define _GEN_ARITH_FUNC_4(op, signFct, unsignFct) _GEN_ARITH_FUNC_X86_4(op, signFct, unsignFct) \
    _GEN_ARITH_FUNC_X86_64_4(op, signFct, unsignFct)
# define _GEN_ARITH_FUNC_DIV_5(op, signFct, unsignFct) _GEN_ARITH_FUNC_X86_DIV_5(op, signFct, unsignFct) \
  _GEN_ARITH_FUNC_X86_64_DIV_5(op, signFct, unsignFct)

#else /* 32b */
# define _GEN_ARITH_FUNC_3(op, signFct, unsignFct) _GEN_ARITH_FUNC_X86_3(op, signFct, unsignFct)
# define _GEN_ARITH_FUNC_DIV_3(op, signFct, unsignFct) _GEN_ARITH_FUNC_X86_DIV_3(op, signFct, unsignFct)
# define _GEN_ARITH_FUNC_MOD_3(op, signFct, unsignFct) _GEN_ARITH_FUNC_X86_MOD_3(op, signFct, unsignFct)
# define _GEN_ARITH_FUNC_4(op, signFct, unsignFct) _GEN_ARITH_FUNC_X86_4(op, signFct, unsignFct)
# define _GEN_ARITH_FUNC_DIV_5(op, signFct, unsignFct) _GEN_ARITH_FUNC_X86_DIV_5(op, signFct, unsignFct)

#endif /* !64b */

/*****************************************************************************/

namespace nativeType
{
  _GEN_ARITH_FUNC_3(add, nativeAddInt, nativeAddUInt)
  _GEN_ARITH_FUNC_3(sub, nativeSubInt, nativeSubUInt)
  _GEN_ARITH_FUNC_3(mul, nativeMultInt, nativeMultUInt)
  _GEN_ARITH_FUNC_4(mul, nativeMultHighInt, nativeMultHighUInt)

  _GEN_ARITH_FUNC_DIV_3(div, nativeDivInt, nativeDivUInt)
  _GEN_ARITH_FUNC_MOD_3(mod, nativeDivInt, nativeDivUInt)
  _GEN_ARITH_FUNC_4(div, nativeDivInt, nativeDivUInt)
  _GEN_ARITH_FUNC_DIV_5(div, nativeDivHighInt, nativeDivHighUInt)

  _GEN_ARITH_FUNC_3(lshift, nativeLShiftInt, nativeLShiftUInt)
  _GEN_ARITH_FUNC_3(rshift, nativeRShiftInt, nativeRShiftUInt)
  _GEN_ARITH_FUNC_3(bitAnd, nativeBitAndInt, nativeBitAndUInt)
  _GEN_ARITH_FUNC_3(bitXor, nativeBitXorInt, nativeBitXorUInt)
  _GEN_ARITH_FUNC_3(bitOr, nativeBitOrInt, nativeBitOrUInt)
} // !namespace nativeType

/*****************************************************************************/

std::ostream &operator<<(std::ostream &a, nativeType::t_eflags const &f)
{
  a << "CF: " << f.s_eflags.CF << " PF: " << f.s_eflags.PF
    << " AF: " << f.s_eflags.AF << " ZF: " << f.s_eflags.ZF << " SF: " << f.s_eflags.SF
    << " TF: " << f.s_eflags.TF << " IF: " << f.s_eflags.IF << " DF: " << f.s_eflags.DF
    << " OF: " << f.s_eflags.OF << " IOPL: " << f.s_eflags.IOPL << " NT: " << f.s_eflags.NT;
  return (a);
}

/*
** For nostalgy, the following code is a small part of what was written before
** the use of the macro. (only the old x86_64 part)
*/

// /* 32b/64b compatibility */
// # if defined(__x86_64__) || defined(__ia64__)
//   template <> uint16_t add(uint64_t a, uint64_t b, uint64_t *result) {
//     return (nativeAddUInt64(a, b, result));}
//   template <> uint16_t add(int64_t a, int64_t b, int64_t *result) {
//     return (nativeAddInt64(a, b, result));}

//   template <> uint16_t sub(int64_t a, int64_t b, int64_t *result) {
//     return (nativeSubInt64(a, b, result));}
//   template <> uint16_t sub(uint64_t a, uint64_t b, uint64_t *result) {
//     return (nativeSubUInt64(a, b, result));}

//   template <> uint16_t mul(int64_t a, int64_t b, int64_t *result) {
//     return (nativeMultInt64(a, b, result));}
//   template <> uint16_t mul(uint64_t a, uint64_t b, uint64_t *result) {
//     return (nativeMultUInt64(a, b, result));}

//   template <> uint16_t mul(int64_t a, int64_t b, int64_t *result, int64_t *high) {
//     return (nativeMultHighInt64(a, b, result, high));}
//   template <> uint16_t mul(uint64_t a, uint64_t b, uint64_t *result, uint64_t *high) {
//     return (nativeMultHighUInt64(a, b, result, high));}

//   template <> uint16_t div(int64_t a, int64_t b, int64_t *quotient) {
//     return (nativeDivInt64(a, b, quotient, NULL));}
//   template <> uint16_t div(int64_t a, int64_t b, int64_t *quotient, int64_t *remainder) {
//     return (nativeDivInt64(a, b, quotient, remainder));}
//   template <> uint16_t div(uint64_t a, uint64_t b, uint64_t *quotient, uint64_t *remainder) {
//     return (nativeDivUInt64(a, b, quotient, remainder));}

//   template <> uint16_t div(uint64_t a, uint64_t b, uint64_t *quotient) {
//     return (nativeDivUInt64(a, b, quotient, NULL));}
//   template <> uint16_t div(int64_t a, int64_t b, int64_t *quotient, int64_t *remainder, int64_t high) {
//     return (nativeDivHighInt64(a, b, quotient, remainder, high));}
//   template <> uint16_t div(uint64_t a, uint64_t b, uint64_t *quotient, uint64_t *remainder, uint64_t high) {
//     return (nativeDivHighUInt64(a, b, quotient, remainder, high));}

//   template <> uint16_t lshift(int64_t a, int64_t b, int64_t *result) {
//     return (nativeLShiftInt64(a, b, result));}
//   template <> uint16_t lshift(uint64_t a, uint64_t b, uint64_t *result) {
//     return (nativeLShiftUInt64(a, b, result));}

//   template <> uint16_t rshift(int64_t a, int64_t b, int64_t *result) {
//     return (nativeRShiftInt64(a, b, result));}
//   template <> uint16_t rshift(uint64_t a, uint64_t b, uint64_t *result) {
//     return (nativeRShiftUInt64(a, b, result));}


//   template <> uint16_t bitAnd(int64_t a, int64_t b, int64_t *result) {
//     return (nativeBitAndInt64(a, b, result));}
//   template <> uint16_t bitAnd(uint64_t a, uint64_t b, uint64_t *result) {
//     return (nativeBitAndUInt64(a, b, result));}
//   template <> uint16_t bitOr(int64_t a, int64_t b, int64_t *result) {
//     return (nativeBitOrInt64(a, b, result));}
//   template <> uint16_t bitOr(uint64_t a, uint64_t b, uint64_t *result) {
//     return (nativeBitOrUInt64(a, b, result));}
//   template <> uint16_t bitXor(int64_t a, int64_t b, int64_t *result) {
//     return (nativeBitXorInt64(a, b, result));}
//   template <> uint16_t bitXor(uint64_t a, uint64_t b, uint64_t *result) {
//     return (nativeBitXorUInt64(a, b, result));}
// # endif
