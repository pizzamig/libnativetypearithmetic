// Copyright (c) 2016, Jean-Baptiste Laurent
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the Barghest Project.
///
/// @brief Unit test for the asm overflow check function
///
/// compile with: g++ -std=c++11 main.cpp  -I ../include/ -L ../build/ -l nativeTypeArithmetic

#include <stdlib.h>
#include <iostream>
#include <limits>
#include <assert.h>
#include <time.h>
#include <stdio.h>
#include "cCppNativeArithmeticWithEflags.hh"

using namespace nativeType;

/*
** Check limit non overflow case
*/
template <typename T>
void        testing_non_overflow_limit_case()
{
  T        res;
  t_eflags    eflags;

  eflags.word = add<T>(std::numeric_limits<T>::max(), 0, &res);assert(getOverflow<T>(eflags) == false && res == std::numeric_limits<T>::max());
  eflags.word = sub<T>(std::numeric_limits<T>::max(), 0, &res);assert(getOverflow<T>(eflags) == false && res == std::numeric_limits<T>::max());
  eflags.word = mul<T>(std::numeric_limits<T>::max(), 0, &res);
  assert(getOverflow<T>(eflags) == false);
  assert(res == 0);

  eflags.word = add<T>(std::numeric_limits<T>::min(), 0, &res);assert(getOverflow<T>(eflags) == false && res == std::numeric_limits<T>::min());
  eflags.word = sub<T>(std::numeric_limits<T>::min(), 0, &res);assert(getOverflow<T>(eflags) == false && res == std::numeric_limits<T>::min());
  eflags.word = add<T>(std::numeric_limits<T>::min(), 0, &res);
  assert(getOverflow<T>(eflags) == false);
  assert(res == std::numeric_limits<T>::min());
  eflags.word = mul<T>(std::numeric_limits<T>::max(), 1, &res);assert(getOverflow<T>(eflags) == false && res == std::numeric_limits<T>::max());
  eflags.word = mul<T>(std::numeric_limits<T>::min(), 1, &res);assert(getOverflow<T>(eflags) == false && res == std::numeric_limits<T>::min());
}

/*
** Check overflow
*/
template <typename T>
void    testing_overflow()
{
  T    res;
  T    i;
  t_eflags    eflags;
  bool    is_signed;

  is_signed = std::is_signed<T>::value;
  eflags.word = add<T>(std::numeric_limits<T>::min(), -1, &res);
  assert(getOverflow<T>(eflags) == is_signed && res == std::numeric_limits<T>::max());
  eflags.word = add<T>(std::numeric_limits<T>::max(), 1, &res);
  if (res != std::numeric_limits<T>::min())
  {
    std::cout << "Overflow: " << std::boolalpha << getOverflow<T>(eflags) << std::endl;
    std::cout << "Error: Have " << res << " / Expected " << std::numeric_limits<T>::min() << std::endl;
    std::cout << "Error: Have " << res << " / Expected2 " << (T)(std::numeric_limits<T>::max() + 1) << std::endl;
  }
  assert(getOverflow<T>(eflags) && res == std::numeric_limits<T>::min());

  eflags.word = add<T>(-1, std::numeric_limits<T>::min(), &res);assert(getOverflow<T>(eflags) == is_signed && res == std::numeric_limits<T>::max());
  eflags.word = add<T>(1, std::numeric_limits<T>::max(), &res);assert(getOverflow<T>(eflags) && res == std::numeric_limits<T>::min());

  if (is_signed == true) // -1 - -128 == -1 + 128 == 127
  {
    eflags.word = sub<T>(-1, std::numeric_limits<T>::min(), &res);assert(getOverflow<T>(eflags) == false);
  }


  for (i = 1 ; i < 120 ; ++i)
  {
    // std::cout << "Testing " << +std::numeric_limits<T>::max() << " + " << +i << " = " << +(T)((T)std::numeric_limits<T>::max() + i) << std::endl;
    eflags.word = add<T>(std::numeric_limits<T>::max(), i, &res);assert(getOverflow<T>(eflags) && res == (T)(std::numeric_limits<T>::max() + i));
    eflags.word = sub<T>(std::numeric_limits<T>::min(), i, &res);assert(getOverflow<T>(eflags) && res == (T)(std::numeric_limits<T>::min() - i));
    eflags.word = mul<T>(std::numeric_limits<T>::max(), i+1, &res);assert(getOverflow<T>(eflags) && res == (T)(std::numeric_limits<T>::max() * (i+1)));
  }
  i = (T)(1) << ((sizeof(T) * 8) - 1); /* Set the bit sign */
  eflags.word = lshift<T>(1, ((sizeof(T) * 8) - 2), &res);
  eflags.word = lshift<T>(res, 1, &res);assert(res == i && eflags.s_eflags.OF == 1 && eflags.s_eflags.CF == 0);
  eflags.word = lshift<T>(res, 1, &res);assert(res == 0 && eflags.s_eflags.OF == 1 && eflags.s_eflags.CF == 1);
  eflags.word = lshift<T>(res, 1, &res);assert(res == 0 && eflags.s_eflags.OF == 0 && eflags.s_eflags.CF == 0);

  i = (T)(3) << ((sizeof(T) * 8) - 2);
  eflags.word = lshift<T>(3, ((sizeof(T) * 8) - 3), &res);  /* Set the two high bits */
  eflags.word = lshift<T>(res, 1, &res);assert(res == i && eflags.s_eflags.OF == 1 && eflags.s_eflags.CF == 0);
  eflags.word = lshift<T>(res, 1, &res);assert(res == (T)(i << 1) && eflags.s_eflags.OF == 0 && eflags.s_eflags.CF == 1);
  eflags.word = lshift<T>(res, 1, &res);assert(res == 0 && eflags.s_eflags.OF == 1 && eflags.s_eflags.CF == 1);
  eflags.word = lshift<T>(res, 1, &res);assert(res == 0 && eflags.s_eflags.OF == 0 && eflags.s_eflags.CF == 0);
}

/*!
** Testing 10 000 times random with random number (++, +-, -+, --) and check with manual operation
*/
template <typename T>
void    testing_result()
{
  int    i, e;
  T    res;
  T    remainder;
  T    random_nb;
  T    random_nb1;
  T    high;

  typedef typename std::conditional<std::is_signed<T>::value, ssize_t, size_t>::type upperType_t;
  auto        check_high_mult_part = [](T random_nb, T random_nb1, T low, T high)
    {
      upperType_t    result_low_high_concat;

      if (sizeof(T) != sizeof(size_t))
      {
        result_low_high_concat = (upperType_t)high;
        result_low_high_concat <<= (sizeof(T) < sizeof(size_t) ? (sizeof(T) * 8) : (assert(0), 0) /* Not used */);
        // Force qualifier cast before the type (in order to not corrupt high byte)
        result_low_high_concat |= (upperType_t)((typename std::make_unsigned<T>::type)low);
        assert(result_low_high_concat == (upperType_t)((upperType_t)random_nb * (upperType_t)random_nb1));
      }
    };
  // Testing we correctly check the null pointer
  add<T>(1, 1);
  add<T>(1, 1, NULL);
  sub<T>(1, 2, NULL);
  mul<T>(1, 2, NULL);
  mul<T>(1, 2, NULL, &high);
  mul<T>(1, 2, &res, NULL);
  mul<T>(1, 2, NULL, NULL);
  div<T>(1, 2, NULL);
  div<T>(1, 2, NULL, &high);
  div<T>(1, 2, &res, NULL);
  div<T>(1, 2, NULL, NULL);
  mod<T>(1, 2, NULL);
  lshift<T>(1, 1);
  rshift<T>(1, 1);
  bitAnd<T>(1, 1);
  bitOr<T>(1, 1);
  bitXor<T>(1, 1);
  srand(time(NULL));
  for (i = 0 ; i < 100000 ; i++)
  {
    for (e = 0 ; e < 4 ; ++e)
    {
      if (e == 0) // ++
      {
        random_nb = rand();
        random_nb1 = rand();
      }
      else if (e == 1) // -+
        random_nb = -random_nb;
      else if (e == 2) // --
        random_nb1 = -random_nb1;
      else // +-
        random_nb = -random_nb;
      add<T>(random_nb, random_nb1, &res);assert(res == (T)(random_nb + random_nb1));
      sub<T>(random_nb, random_nb1, &res);assert(res == (T)(random_nb - random_nb1));
      mul<T>(random_nb, random_nb1, &res);assert(res == (T)(random_nb * random_nb1));
      mul<T>(random_nb, random_nb1, &res, &high);assert(res == (T)(random_nb * random_nb1));
      check_high_mult_part(random_nb, random_nb1, res, high);
      if (random_nb1 && !(std::is_signed<T>::value // This whould cause a #DE floating point error because of the overflow
                          && random_nb == std::numeric_limits<T>::min() && random_nb1 == (T)~0/*-1*/))
      {
        div<T>(random_nb, random_nb1, &res, &remainder);
        assert(res == (T)(random_nb / random_nb1));
        assert(remainder == (T)(random_nb % random_nb1));
        high = 0;
        if (std::is_signed<T>::value && (random_nb >> ((sizeof(T) * 8) - 1)) != 0) // is negative
          high = -1;
        div<T>(random_nb, random_nb1, &res, &remainder, high);
        assert(res == (T)(random_nb / random_nb1));
        assert(remainder == (T)(random_nb % random_nb1));
        /**/
        mod<T>(random_nb, random_nb1, &res);assert(res == (T)(random_nb % random_nb1));
      }
      lshift<T>(random_nb, random_nb1, &res);assert(res == (T)(random_nb << random_nb1));
      rshift<T>(random_nb, random_nb1, &res);assert(res == (T)(random_nb >> random_nb1));
      bitAnd<T>(random_nb, random_nb1, &res);assert(res == (T)(random_nb & random_nb1));
      bitOr<T>(random_nb, random_nb1, &res);assert(res == (T)(random_nb | random_nb1));
      bitXor<T>(random_nb, random_nb1, &res);assert(res == (T)(random_nb ^ random_nb1));
    }
  }
}

template <typename T>
void    testing_flags()
{
  t_eflags    eflags;
  T        res;

  // eflags.word = sub<T>(1, std::numeric_limits<T>::min(), &res);
  // std::cout << eflags << std::endl;
  if (sizeof(T) == 1 && std::is_signed<T>::value)
  {
    eflags.word = sub<T>(1, -128, &res);
    std::cout << "1 - -128: " << eflags << std::endl;
    eflags.word = add<T>(127, 1, &res);
    std::cout << " 127 + 1: " << eflags << std::endl;
    eflags.word = mul<T>(64, 2, &res);
    std::cout << "S 64 * 2: " << eflags << std::endl;
  }
  if (sizeof(T) == 1 && std::is_unsigned<T>::value)
  {
    eflags.word = mul<T>(64, 2, &res);
    std::cout << "U 64 * 2: " << eflags << std::endl;
  }
  // assert(eflags.ZF == 0 && eflags.s_eflags.SF == eflags.s_eflags.OF);
}

template <typename T>
void    testing()
{
  testing_non_overflow_limit_case<T>();
  testing_overflow<T>();
  testing_result<T>();
  testing_flags<T>();
}

int    main()
{
  std::cout << "Starting test" << std::endl;
  std::cout << "==> testing<int8_t>();" << std::endl;
  testing<int8_t>();
  std::cout << "==> testing<uint8_t>();" << std::endl;
  testing<uint8_t>();
  std::cout << "==> testing<int16_t>();" << std::endl;
  testing<int16_t>();
  std::cout << "==> testing<uint16_t>();" << std::endl;
  testing<uint16_t>();
  std::cout << "==> testing<int32_t>();" << std::endl;
  testing<int32_t>();
  std::cout << "==> testing<uint32_t>();" << std::endl;
  testing<uint32_t>();

/* 32b/64b compatibility */
# if defined(__x86_64__) || defined(__ia64__)
  std::cout << "==> testing<int64_t>();" << std::endl;
  testing<int64_t>();
  std::cout << "==> testing<uint64_t>();" << std::endl;
  testing<uint64_t>();
#endif
  std::cout << "All test OK" << std::endl;
  return (0);
}
